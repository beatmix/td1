package service;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import com.example.iproger.td1.R;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import activity.MenuOfPersonActivity;
import adapter.PersonsMenuAdapter;
import model.PMenu;
import model.Person;

/**
 * Created by user on 25.01.2017.
 */

public class GetMenusForPersonTask extends AsyncTask<Void, Void, String> {

    Context context;
    ListView productList;
    MenuOfPersonActivity parentActivity;
    Person currentPerson;
    PersonsMenuAdapter customAdapter;

    public GetMenusForPersonTask(Context context, ListView productList, MenuOfPersonActivity parentActivity, Person currentPerson) {
        this.context = context;
        this.productList = productList;
        this.parentActivity = parentActivity;
        this.currentPerson = currentPerson;
    }


    @Override
    protected String doInBackground(Void... params) {

        StringBuffer result = null;
        String response1 = null;
        try {

            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler response = new BasicResponseHandler();
            HttpGet http = new HttpGet(context.getResources().getString(R.string.serverIP) + "/person/menus/" + currentPerson.getId());

            response1 = (String) hc.execute(http, response);

        } catch (Exception e) {

        }

        return response1;


    }

    @Override
    protected void onPostExecute(String success) {

        //    System.out.println(7);


       // List<String> menus = new ArrayList<>();
        List <PMenu> menus = new ArrayList<>();
        try {
            try {
                JSONObject jObj = new JSONObject(success);
                JSONArray menusJson = jObj.getJSONArray("menus");


                for (int i = 0; i < menusJson.length(); i++) {
                    JSONObject obj = menusJson.getJSONObject(i);
                    PMenu menu = new PMenu();
                    menu.setPrice(Integer.parseInt(obj.getString("price")));


                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d = sdf.parse(obj.getString("createdAt"));
                    String formattedTime = output.format(d);


                    //System.out.println("CCREATED >>>>>>>>  "+formattedTime);
                    menu.setCreatedAt(d);
                    menu.setDiscount(Double.parseDouble(obj.getString("discount")));
                    menu.setId(obj.getString("id"));
                    JSONObject server = obj.getJSONObject("server");
                    menu.setServerId(server.getString("id"));

                    JSONArray itemsJson = obj.getJSONArray("items");
                    List<String> items = new ArrayList<>();
                    for (int j = 0; j < itemsJson.length(); j++) {
                        JSONObject obj2 = itemsJson.getJSONObject(j);
                        items.add(obj2.getString("id"));

                    }
                    menu.setItems(items);
                    menus.add(menu);


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

         customAdapter = new PersonsMenuAdapter(parentActivity.getApplicationContext(), menus, parentActivity);
          productList.setAdapter(customAdapter);
          productList.setItemsCanFocus(false);


    }


    @Override
    protected void onCancelled() {

    }



}
