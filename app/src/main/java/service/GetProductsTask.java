package service;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import com.example.iproger.td1.R;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import activity.ProductsViewActivity;
import adapter.ProductItemAdapter;
import model.Product;

public class GetProductsTask extends AsyncTask<Void, Void, String> {

    Context context;
    ListView productList;
    ProductsViewActivity parentActivity;
    ProductItemAdapter customAdapter;

    public GetProductsTask(Context context, ListView productList, ProductsViewActivity parentActivity ){
        this.context = context;
        this.productList = productList;
        this.parentActivity = parentActivity;
    }


    @Override
    protected String doInBackground(Void... params) {

        StringBuffer result = null;
        String response1 = null;
        try {

            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler response = new BasicResponseHandler();
            HttpGet http = new HttpGet(context.getResources().getString(R.string.serverIP)+"/product");

            response1 = (String) hc.execute(http, response);

        } catch (Exception e) {

        }

        String reponseDuWebService;
        return response1;


    }

    @Override
    protected void onPostExecute(String success) {

        //    System.out.println(7);


        List<Product> products = new ArrayList<>();
        try {
            try {
                JSONArray array = new JSONArray(success);
                for (int i = 0; i < array.length(); i++) {
                    try {
                        JSONObject ob = array.getJSONObject(i);

                        Product p = new Product();
                        p.setId(ob.getString("id"));
                        p.setName(ob.getString("name"));
                        p.setDescription(ob.getString("description"));
                        p.setPrice(ob.getInt("price"));
                        p.setCalories(ob.getInt("calories"));
                        p.setType(ob.getString("type"));
                        p.setPicture(ob.getString("picture"));
                        p.setDiscount(ob.getInt("discount"));

                        products.add(p);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        customAdapter = new ProductItemAdapter(parentActivity.getApplicationContext(), products, parentActivity);
        productList.setAdapter(customAdapter);
        productList.setItemsCanFocus(false);


    }


    @Override
    protected void onCancelled() {

    }

}