package service;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.TextView;

import com.example.iproger.td1.R;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import activity.ProductsViewActivity;
import adapter.ProductItemAdapter;
import model.Product;

public class GetProductByIdTask extends AsyncTask<String, Void, String> {

    Context context;
    TextView textView;

    public GetProductByIdTask(Context context, TextView textView){
        this.context = context;
        this.textView = textView;
    }


    @Override
    protected String doInBackground(String... params) {

        String id = params[0];

        StringBuffer result = null;
        String response1 = null;
        try {

            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler response = new BasicResponseHandler();
            HttpGet http = new HttpGet(context.getResources().getString(R.string.serverIP)+"/product/"+id);

            response1 = (String) hc.execute(http, response);

        } catch (Exception e) {

        }

        String reponseDuWebService;
        return response1;


    }

    @Override
    protected void onPostExecute(String success) {

        //    System.out.println(7);

        try {
            try {

                        JSONObject ob = new JSONObject(success);

                        Product p = new Product();
                        p.setId(ob.getString("id"));
                        p.setName(ob.getString("name"));
                        p.setDescription(ob.getString("description"));
                        p.setPrice(ob.getInt("price"));
                        p.setCalories(ob.getInt("calories"));
                        p.setType(ob.getString("type"));
                        p.setPicture(ob.getString("picture"));
                        p.setDiscount(ob.getInt("discount"));

                textView.setText(p.getName());


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    @Override
    protected void onCancelled() {

    }

}