package service;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.iproger.td1.R;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import activity.GroupViewActivity;

public class DeletePersonTask extends AsyncTask<Void, Void, String> {

    private final String mId;
    Context context;
    GroupViewActivity parentActivity;


    public DeletePersonTask(String id, Context context, GroupViewActivity parentActivity) {
        this.mId = id;
        this.context = context;
        this.parentActivity = parentActivity;
    }

    @Override
    protected String doInBackground(Void... params) {

        StringBuffer result = null;
        String response1 = null;
        try {

            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler response = new BasicResponseHandler();
            HttpDelete http = new HttpDelete(context.getResources().getString(R.string.serverIP)+"/person/" + mId);

            response1 = (String) hc.execute(http, response);

        } catch (Exception e) {

        }

        String reponseDuWebService;
        return response1;


    }

    @Override
    protected void onPostExecute(String success) {
        System.out.println(success);
        Toast.makeText(parentActivity, context.getResources().getString(R.string.saved), Toast.LENGTH_LONG).show();

    }


    @Override
    protected void onCancelled() {

    }

}




