package service;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.iproger.td1.R;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import activity.GroupViewActivity;
import adapter.PersonItemAdapter;
import model.Person;

/**
 * Created by user on 25.01.2017.
 */

public class GetPersonsTask extends AsyncTask<Void, Void, String> {

    Person curPerson;
    Context context;
    ListView personList;
    PersonItemAdapter customAdapter;
    GroupViewActivity parentActivity;

    public GetPersonsTask(Context context, Person currentPerson, ListView personList, GroupViewActivity parentActivity) {
        this.context = context;
        this.curPerson = currentPerson;
        this.personList = personList;
        this.parentActivity = parentActivity;
    }

    @Override
    protected String doInBackground(Void... params) {

        StringBuffer result = null;
        String response1 = null;
        try {

            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler response = new BasicResponseHandler();
            HttpGet http = new HttpGet(context.getResources().getString(R.string.serverIP)+"/person");

            response1 = (String) hc.execute(http, response);

        } catch (Exception e) {

        }

        String reponseDuWebService;
        return response1;


    }

    @Override
    protected void onPostExecute(String success) {

        //    ListView lst = (ListView) findViewById(R.id.list_person);




        List<Person> persons = new ArrayList<>();
        try {
            try {
                JSONArray array = new JSONArray(success);
                for (int i = 0; i < array.length(); i++) {
                    try {
                        JSONObject ob = array.getJSONObject(i);
                        Person p = new Person();
                        p.setId(ob.getString("id"));
                        p.setNom(ob.getString("nom"));
                        p.setPrenom(ob.getString("prenom"));
                        p.setSexe(ob.getString("sexe"));
                        p.setTelephone(ob.getString("telephone"));
                        p.setEmail(ob.getString("email"));
                        p.setPassword(ob.getString("password"));
                        p.setCreatedby(ob.getString("createdby"));
                        p.setConnected(ob.getString("connected"));

                        persons.add(p);

                    } catch (Exception e) {

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        customAdapter = new PersonItemAdapter(context.getApplicationContext(), persons,parentActivity, curPerson);
        personList.setAdapter(customAdapter);
        personList.setItemsCanFocus(false);
        personList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                Person clickedObj = (Person) parent.getItemAtPosition(position);
                Toast.makeText(parentActivity,
                        "Clicked item:\n" +
                                clickedObj.getFullName(),
                        Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    protected void onCancelled() {

    }

}
