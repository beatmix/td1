package service;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.iproger.td1.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import activity.MenuActivity;

/**
 * Created by user on 25.01.2017.
 */
public class SendMenuTask extends AsyncTask<JSONObject, String, String> {

    Context context;
    MenuActivity parentActivity;

    public SendMenuTask (Context context, MenuActivity parentActivity){
        this.context = context;
        this.parentActivity = parentActivity;
    }


    @Override
    protected String doInBackground(JSONObject... params) {

        String url = context.getResources().getString(R.string.serverIP)+"/menu/";

        JSONObject json = params[0];
        StringBuffer result = null;
        if (json!=null)
            System.out.println("JSON >>>>>" + json);
        else
            System.out.println("JSON NULL >>>>>");

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            // add header
            post.setHeader("Content-Type", "application/json");


            StringEntity entity = new StringEntity(json.toString());

            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

               /* BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
                result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }*/

            // return result.toString();
        } catch (Exception e) {

        }

        return "";


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        parentActivity.showProgressDialog(true);

    }

    @Override
    protected void onPostExecute(String result) {
        parentActivity.showProgressDialog(false);
        Toast.makeText(context,"Menu has sent",Toast.LENGTH_SHORT).show();
    }


}

