package service;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 25.01.2017.
 */

public class BitmapToCacheSaver {

    public static Map<String, Bitmap> cache;

    public BitmapToCacheSaver(){
        cache = new HashMap<String, Bitmap>();
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            cache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return cache.get(key);
    }
}
