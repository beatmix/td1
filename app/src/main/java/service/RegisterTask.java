package service;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.iproger.td1.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import activity.LoginActivity;
import activity.RegistrationActivity;
import model.Person;

/**
 * Created by user on 25.01.2017.
 */

public class RegisterTask extends AsyncTask<Person, Void, String> {

    Context context;
    RegistrationActivity parentActivity;

    public RegisterTask(Context context, RegistrationActivity parentActivity){
        this.context = context;
        this.parentActivity = parentActivity;
    }




    @Override
    protected String doInBackground(Person... people) {
        String url = context.getResources().getString(R.string.serverIP)+"/person/";
        Person person = people[0];
        StringBuffer result = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            // add header

            post.setHeader("Content-Type", "application/json");
            JSONObject obj = new JSONObject();
            obj.put("prenom", person.getNom());
            obj.put("nom", person.getPrenom());
            obj.put("sexe", person.getSexe());
            obj.put("telephone", person.getTelephone());
            obj.put("email", person.getEmail());
            obj.put("createdby", person.getCreatedby());
            obj.put("password", person.getPassword());


            System.out.println(obj.toString());
            StringEntity entity = new StringEntity(obj.toString());

            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            // return result.toString();
        } catch (Exception e) {

        }

        String reponseDuWebService;
        return result.toString();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        parentActivity.showProgressDialog(true);

    }

    @Override
    protected void onPostExecute(String result) {
        String s=null;
        if (result != null) {

            try {
                JSONObject jsonObj = new JSONObject(result);
                s = jsonObj.getString("id");

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (s!=null) {

                Intent intent = new Intent(parentActivity, LoginActivity.class);
                context.startActivity(intent);
            }
            else
                Toast.makeText(parentActivity.getApplicationContext(), "User is not registered on the server", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(parentActivity.getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
        }
        parentActivity.showProgressDialog(false);




    }


}