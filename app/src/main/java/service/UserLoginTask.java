package service;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.iproger.td1.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;

import activity.LoginActivity;
import activity.WelcomeActivity;
import model.Person;

/**
 * Created by user on 25.01.2017.
 */

public class UserLoginTask extends AsyncTask<Void, Void, String> {
    //  String url = "http://95.142.161.35:1337/person/login";

    private final String mEmail;
    private final String mPassword;
    Context context;
    LoginActivity parentActivity;

    public UserLoginTask(String email, String password, Context context, LoginActivity parentActivity) {
        mEmail = email;
        mPassword = password;
        this.context = context;
        this.parentActivity = parentActivity;
    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO: attempt authentication against a network service.
        System.out.println(mEmail);
        System.out.println(params[0]);
        StringBuffer result = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(context.getResources().getString(R.string.serverIP)+"/person/login");

            // add header

            post.setHeader("Content-Type", "application/json");
            JSONObject obj = new JSONObject();
            obj.put("email", mEmail);
            obj.put("password", mPassword);


            System.out.println(obj.toString());
            StringEntity entity = new StringEntity(obj.toString());

            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            System.out.println("\nSending 'POST' request to URL : " + context.getResources().getString(R.string.serverIP) +"/person/login");
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " +
                    response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            // return result.toString();
        } catch (Exception e) {

        }

        String reponseDuWebService;
        return result.toString();


    }

    @Override
    protected void onPostExecute(String success) {
        //mAuthTask = null;

//send token to app server
        parentActivity.showProgress(false);
        System.out.println(success);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(success);
            //   jsonObj = jsonObj.getJSONObject("success");
            //   String s = jsonObj.toString();
            boolean multipleContacts = jsonObj.getBoolean("success");

            if (multipleContacts==true) {
                jsonObj = jsonObj.getJSONObject("user");

                Person personV = gson.fromJson(String.valueOf(jsonObj), Person.class);
                Intent intent = new Intent(parentActivity, WelcomeActivity.class);
                intent.putExtra("person_object", (Serializable) personV);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
            else
            if (multipleContacts==false){
                // jsonObj = jsonObj.getJSONObject("message");
                String s = jsonObj.getString("message");
                Toast.makeText(parentActivity,
                        s,
                        Toast.LENGTH_LONG).show();
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

         /*   if (success != null) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }*/
    }

    @Override
    protected void onCancelled() {
       // mAuthTask = null;

        parentActivity.showProgress(false);
    }

}