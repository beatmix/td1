package service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import activity.MenuActivity;
import adapter.BaseAdapterForImages;
import adapter.MenuItemAdapter;

/**
 * Created by user on 25.01.2017.
 */

public class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

    private String url;
    private ImageView imageView;
    BaseAdapterForImages adapter;
    Map<String,Bitmap> cache;

    public ImageLoadTask(String url, ImageView imageView, BaseAdapterForImages adapter, Map<String,Bitmap> cache) {
        this.url = url;
        this.imageView = imageView;
        this.adapter = adapter;
        this.cache = cache;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            URL urlConnection = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlConnection
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        imageView.setImageBitmap(result);

        adapter.addBitmapToMemoryCache(url,result, cache);
        //saver.addBitmapToMemoryCache(url, result);
    }

}