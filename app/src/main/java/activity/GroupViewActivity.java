package activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iproger.td1.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import activity.RegistrationActivity;
import adapter.PersonItemAdapter;
import model.Person;
import service.DeletePersonTask;
import service.GetPersonsTask;

/**
 * Created by user on 14.11.2016.
 */

public class GroupViewActivity extends AppCompatActivity {
    public List<Person> persons;
    ListView personList;
    private GetPersonsTask mAuthTask = null;
    ImageButton btn;
    Button btn2;
    final String LOG_TAG = "myLogs";
    Context context;
    PersonItemAdapter customAdapter;
    Person currentPerson;
    GroupViewActivity parentActivity;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupview);
        context = this.getApplicationContext();
        parentActivity = GroupViewActivity.this;

        Intent i = getIntent();
        Person currentPerson = (Person) i.getSerializableExtra("person_object");
        personList = (ListView) findViewById(R.id.list_person);


        btn2 = (Button) findViewById(R.id.button_post);

        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(GroupViewActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });

        mAuthTask = new GetPersonsTask(context, currentPerson, personList, parentActivity);
        mAuthTask.execute((Void) null);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void deletePerson(final String id, PersonItemAdapter customAdapterTemp, List<Person> personsList) {

        Person person = new Person();
        for (Person p : personsList) {
            if (p.getId() == id)
                person = p;
        }
        personsList.remove(person);
        customAdapterTemp.refresh(personsList);
        DeletePersonTask mTask;
        mTask = new DeletePersonTask(id, context, parentActivity);
        mTask.execute((Void) null);

    }

    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Buzz is sending...");
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }

}




