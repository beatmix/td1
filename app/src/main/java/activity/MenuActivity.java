package activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import adapter.MenuItemAdapter;
import com.example.iproger.td1.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.List;
import java.util.StringTokenizer;

import adapter.PersonItemAdapter;
import adapter.ProductItemAdapter;
import model.Person;
import model.Product;
import service.DeletePersonTask;
import service.SendMenuTask;

/**
 * Created by user on 22.11.2016.
 */

public class MenuActivity extends AppCompatActivity {
    MenuItemAdapter customAdapter;
    ListView entreList, platsList, dessertsList;
    List<Product> entrees, plats, desserts;
    TextView price, discount;
    int totalPrice;
    double totalDiscount;
    List<Product> menu;
    Button btn;
    Person currentPerson;
    ProgressDialog progressDialog;


    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuview);
        totalPrice = 0;
        totalDiscount = 0.0;
        Intent i = getIntent();
        menu = (List<Product>) i.getSerializableExtra("menu_object");

        currentPerson = (Person) i.getSerializableExtra("person_object");

        for (Product p : menu) {
            totalPrice += p.getPrice();
            totalDiscount += (double) p.getPrice() * ( (double)p.getDiscount()/100);
        }
        price = (TextView) findViewById(R.id.price);
       discount = (TextView) findViewById(R.id.discount);
        discount.setText(String.valueOf(totalDiscount));


        discount.setText("Total discount = "+ String.valueOf(totalDiscount));
        price.setText("Total Price = "+String.valueOf(totalPrice));

        entreList = (ListView) findViewById(R.id.list_entrees);

        customAdapter = new MenuItemAdapter(getApplicationContext(), menu, MenuActivity.this);
        entreList.setAdapter(customAdapter);
        entreList.setItemsCanFocus(false);

        btn = (Button)findViewById(R.id.sendMenuBtn);

        context = this.getApplicationContext();

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                JSONObject jObject = menuJson();
                AsyncTask tache = new SendMenuTask(context, MenuActivity.this).execute(jObject);


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void deleteProduct(final String id, MenuItemAdapter customAdapterTemp, List<Product> productsList) {

        Product product = new Product();
        for (Product p : productsList) {
            if (p.getId() == id)
                product = p;
        }
        productsList.remove(product);
        customAdapterTemp.refresh(productsList);
        menu = productsList;
        refreshSum();

    }

    public void refreshSum(){
        totalPrice=0;
        totalDiscount=0;
        for (Product p : menu) {
            totalPrice += p.getPrice();
            totalDiscount += (double) p.getPrice() * ( (double)p.getDiscount()/100);
        }
        discount.setText("Total discount = "+ String.valueOf(totalDiscount));
        price.setText("Total Price = "+String.valueOf(totalPrice));
    }

    public JSONObject menuJson(){
        JSONArray array = new JSONArray();

        try {
        for (Product p : menu) {

            JSONObject obj = new JSONObject();
                obj.put("id", p.getId());

                array.put(obj);

        }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject jObject = new JSONObject();
        try {
            JSONObject server = new JSONObject();
            server.put("id",currentPerson.getId());
            jObject.put("server", server);
            jObject.put("price", totalPrice);
            jObject.put("discount",  totalDiscount);
            jObject.put("items", array);

            return jObject;


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void showProgressDialog(boolean isVisible) {
        if (isVisible) {
            if(progressDialog==null) {
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Menu is sending...");
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        progressDialog = null;
                    }
                });
                progressDialog.show();
            }
        }
        else {
            if(progressDialog!=null) {
                progressDialog.dismiss();
            }
        }
    }


}
