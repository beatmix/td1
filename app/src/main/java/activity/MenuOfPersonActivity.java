package activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.iproger.td1.R;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.Person;
import model.Product;
import service.GetMenusForPersonTask;
import service.GetProductsTask;

/**
 * Created by user on 25.01.2017.
 */

public class MenuOfPersonActivity extends AppCompatActivity {

    Person currentPerson;
    SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personsmenuview);

        mPrefs = getPreferences(MODE_PRIVATE);
        Intent i = getIntent();
        currentPerson = (Person) i.getSerializableExtra("person_object");
        if (currentPerson!=null)
            savePrsn(currentPerson);
        else
            currentPerson = loadPerson();

        Context context = this.getApplicationContext();

        ListView listView= (ListView) findViewById(R.id.menuOfPersonList);
        MenuOfPersonActivity parentActivity = MenuOfPersonActivity.this;
        GetMenusForPersonTask mAuthTask = new GetMenusForPersonTask(context, listView, parentActivity, currentPerson);
        mAuthTask.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void savePrsn(Person person){
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(person);
        prefsEditor.putString("CurrPers", json);
        prefsEditor.commit();
    }
    public Person loadPerson(){
        Gson gson = new Gson();
        String json = mPrefs.getString("CurrPers", "");
        Person person = gson.fromJson(json, Person.class);
        return person;
    }


}
