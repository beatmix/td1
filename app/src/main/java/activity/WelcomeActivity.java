package activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import model.Person;

import com.example.iproger.td1.R;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by user on 19.10.2016.
 */

public class WelcomeActivity extends AppCompatActivity {

    Button btn, btn2, btn3;
    SharedPreferences  mPrefs;
    Person p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mPrefs = getPreferences(MODE_PRIVATE);
        Intent i = getIntent();
        p = (Person) i.getSerializableExtra("person_object");
        if (p!=null)
        savePrsn(p);
        else
        p = loadPerson();
        TextView tv = (TextView) findViewById(R.id.textView7);
        tv.setText(p.getNom() + " " + p.getPrenom());

        btn = (Button)findViewById(R.id.buttonListServeurs);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeActivity.this, GroupViewActivity.class);
                intent.putExtra("person_object", (Serializable) p);
                startActivity(intent);
            }
        });

        btn2 = (Button)findViewById(R.id.listMenuButton);
        btn2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeActivity.this, ProductsViewActivity.class);
                intent.putExtra("person_object", (Serializable) p);
                startActivity(intent);
            }
        });

        btn3 = (Button)findViewById(R.id.personMenuListButton);
        btn3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeActivity.this, MenuOfPersonActivity.class);
                intent.putExtra("person_object", (Serializable) p);
                startActivity(intent);
            }
        });
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void savePrsn(Person person){
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(person);
        prefsEditor.putString("CurrPers", json);
        prefsEditor.commit();
    }
    public Person loadPerson(){
        Gson gson = new Gson();
        String json = mPrefs.getString("CurrPers", "");
        Person person = gson.fromJson(json, Person.class);
        return person;
    }


}
