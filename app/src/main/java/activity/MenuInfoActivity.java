package activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.iproger.td1.R;

import adapter.ProductOfPersonMenuAdapter;
import model.PMenu;

/**
 * Created by user on 25.01.2017.
 */

public class MenuInfoActivity extends AppCompatActivity {

    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuinfo);

        Intent i = getIntent();
        context = this.getApplicationContext();
        PMenu m = (PMenu) i.getSerializableExtra("menu_object");

        TextView title = (TextView) findViewById(R.id.title);
        title.setText(m.getCreatedAt().toString());

        TextView id = (TextView) findViewById(R.id.id);
        id.setText("ID = ");
        EditText idEdit = (EditText) findViewById(R.id.editTextId);
        idEdit.setText(m.getId());

        TextView price = (TextView) findViewById(R.id.price);
        price.setText("PRICE = ");
        EditText priceEdit = (EditText) findViewById(R.id.editTextPrice);
        priceEdit.setText(String.valueOf(m.getPrice()));

        TextView discount = (TextView) findViewById(R.id.discount);
        discount.setText("DISCOUNT = ");
        EditText discountEdit = (EditText) findViewById(R.id.editTextDiscount);
        discountEdit.setText(String.valueOf(m.getDiscount()));

        TextView serverId = (TextView) findViewById(R.id.serverId);
        serverId.setText("SERVER ID = ");
        EditText serverEdit = (EditText) findViewById(R.id.editTextServerId);
        serverEdit.setText(String.valueOf(m.getServerId()));

        ListView itemsList = (ListView) findViewById(R.id.itemsList);
        ProductOfPersonMenuAdapter customAdapter = new ProductOfPersonMenuAdapter(context.getApplicationContext(), m.getItems());
        itemsList.setAdapter(customAdapter);
        itemsList.setItemsCanFocus(false);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
