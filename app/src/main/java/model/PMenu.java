package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 25.01.2017.
 */

public class PMenu implements Serializable {

    String id;
    String serverId;
    int price;
    double discount;
    Date createdAt;
    List<String> items;

    public PMenu(List<String> items, String id, String serverId, int price, double discount, Date createdAt) {
        this.items = items;
        this.id = id;
        this.serverId = serverId;
        this.price = price;
        this.discount = discount;
        this.createdAt = createdAt;
    }

    public PMenu() {

    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getAnnounce(){
        return "Price: "+String.valueOf(price)+"/ Discount: "+String.valueOf(discount);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
