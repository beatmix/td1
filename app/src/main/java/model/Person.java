package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by user on 21.10.2016.
 */
public class Person implements Serializable{

    String id;
    String nom;
    String prenom;
    String sexe;
    String telephone;
    String email;
    String password;
    String createdby;
    String connected;
    boolean called;

    // FOR GCM

    String gcmKey;

    public String getGcmKey() {
        return gcmKey;
    }

    public void setGcmKey(String gcmKey) {
        this.gcmKey = gcmKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isCalled() {
        return called;
    }

    public void setCalled(boolean called) {
        this.called = called;
    }

    public String getConnected() {
        return connected;
    }

    public void setConnected(String connected) {
        this.connected = connected;
    }

    public Person(){

    }
    public Person(String nom, String prenom, String sex, String telephone, String email, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sex;
        this.telephone = telephone;
        this.email = email;
        this.password = password;
        this.createdby = "IrinaSergei";
        this.called=false;
    }

    public Person(String id, String nom, String prenom, String sex, String telephone, String email, String password, String connected) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sex;
        this.telephone = telephone;
        this.email = email;
        this.password = password;
        this.createdby = "IrinaSergei";
        this.connected=connected;
        this.called=false;

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getFullName(){
        return nom+" "+prenom;
    }

    public String isConnected() {
        String response = "";
        if (connected.equalsIgnoreCase("true"))
            return "Connecté";
        else
            return "Deconnecté";
    }

    public JSONObject makeTheJSON() {
        JSONObject obj = new JSONObject();
        try {

            obj.put("prenom", getNom());
            obj.put("nom", getPrenom());
            obj.put("sexe", getSexe());
            obj.put("telephone", getTelephone());
            obj.put("email", getEmail());
            obj.put("createdby", getCreatedby());
            obj.put("password", getPassword());
            obj.put("connected", getConnected());
            obj.put("id", getId());
            return obj;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }
}
