package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.iproger.td1.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import activity.MenuActivity;
import model.Product;
import service.BitmapToCacheSaver;
import service.ImageLoadTask;

/**
 * Created by user on 14.11.2016.
 */


public class MenuItemAdapter extends BaseAdapterForImages {

    Context context;
    List<Product> products;
    public AlertDialog.Builder ad;
    MenuActivity parentActivity;
    MenuItemAdapter adapter;

    LayoutInflater inflter;
    public static Map<String, Bitmap> cache;


    public MenuItemAdapter(Context applicationContext, List<Product> products, MenuActivity parentActivity) {
        this.context = applicationContext;
        this.products = products;
        this.parentActivity = parentActivity;
        inflter = (LayoutInflater.from(applicationContext));
        cache = new HashMap<String, Bitmap>();


    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.menu_item2, null);
        TextView name = (TextView) view.findViewById(R.id.name);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView3);
        Button ib = (Button) view.findViewById(R.id.button_supp);
        name.setText(products.get(i).getName());
//       name.setText(products.get(i).getDiscount());
        ib.setTag(i);
        if (getBitmapFromMemCache(products.get(i).getPicture(), cache)==null) {
            adapter = MenuItemAdapter.this;
            AsyncTask<Void, Void, Bitmap> task = new ImageLoadTask(products.get(i).getPicture(), imageView, adapter, cache);
            task.execute();
        }
        else {
            imageView.setImageBitmap(getBitmapFromMemCache(products.get(i).getPicture(), cache));
        }


        ib.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                final String idP = products.get((int)view.getTag()).getId();
                ad = new AlertDialog.Builder(parentActivity);
                String title = context.getResources().getString(R.string.exit2);
                String message = context.getResources().getString(R.string.save_data);;
                String button1String = context.getResources().getString(R.string.yes);;
                String button2String =context.getResources().getString(R.string.no);;


                ad.setTitle(title);
                ad.setMessage(message);
                ad.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {

                        parentActivity.deleteProduct(idP, MenuItemAdapter.this, products);
                    }
                }).setNegativeButton(button2String, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {

                    }
                });
                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                ad.show();

            }
        });
        return view;

    }


    public void refresh(List<Product> products2)
    {
        this.products = products2;
        notifyDataSetChanged();
    }
}




