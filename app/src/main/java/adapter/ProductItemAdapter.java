package adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iproger.td1.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import activity.ProductsViewActivity;
import model.Product;
import service.BitmapToCacheSaver;
import service.ImageLoadTask;

/**
 * Created by user on 14.11.2016.
 */


public class ProductItemAdapter extends BaseAdapterForImages {

    Context context;
    List<Product> products;

    ProductsViewActivity parentActivity;

    LayoutInflater inflter;
    public static Map<String, Bitmap> cache;

    //BitmapToCacheSaver saver;

    public ProductItemAdapter(Context applicationContext, List<Product> products, ProductsViewActivity parentActivity) {
        this.context = applicationContext;
        this.products = products;
        this.parentActivity = parentActivity;
        inflter = (LayoutInflater.from(applicationContext));
         cache = new HashMap<String, Bitmap>();
      //  saver = new BitmapToCacheSaver();

    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.product_item2, null);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView description = (TextView) view.findViewById(R.id.description);
        TextView price = (TextView) view.findViewById(R.id.price);
        TextView calories = (TextView) view.findViewById(R.id.calories);
       TextView type = (TextView) view.findViewById(R.id.type);
        TextView discount = (TextView) view.findViewById(R.id.discount);
       name.setText(products.get(i).getName());

        description.setText(products.get(i).getDescription());
        price.setText(String.valueOf(products.get(i).getPrice()));
        calories.setText(String.valueOf(products.get(i).getCalories()));
        type.setText(products.get(i).getType());
       // System.out.println(products.get(i).getType());
        discount.setText(String.valueOf("-"+products.get(i).getDiscount()+"%"));
        Button btn = (Button) view.findViewById(R.id.button_ajout);
        btn.setTag(i);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView3);

       // System.out.println(products.get(i).getPicture());
        if (getBitmapFromMemCache(products.get(i).getPicture(), cache)==null) {
          new ImageLoadTask(products.get(i).getPicture(), imageView, ProductItemAdapter.this, cache).execute();

        }
        else {
            imageView.setImageBitmap(getBitmapFromMemCache(products.get(i).getPicture(), cache));
        }


        btn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Toast.makeText(context, products.get((int)view.getTag()).getName(), Toast.LENGTH_SHORT).show();
                parentActivity.addProductToMenu(products.get((int)view.getTag()));
            }
        });
        return view;
    }


}

