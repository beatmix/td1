package adapter;

import android.graphics.Bitmap;
import android.widget.BaseAdapter;

import java.util.Map;

/**
 * Created by user on 25.01.2017.
 */

public abstract class BaseAdapterForImages extends BaseAdapter {



    public Map<String, Bitmap> addBitmapToMemoryCache(String key, Bitmap bitmap, Map<String, Bitmap> cache) {
        if (cache.get(key) == null) {
            cache.put(key, bitmap);
        }

        return cache;
    }

    public Bitmap getBitmapFromMemCache(String key, Map<String, Bitmap> cache) {
        return cache.get(key);
    }


}
