package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.iproger.td1.R;

import java.util.List;

import model.PMenu;
import service.GetProductByIdTask;
import service.GetProductsTask;

/**
 * Created by user on 25.01.2017.
 */

public class ProductOfPersonMenuAdapter extends BaseAdapter  {
    LayoutInflater inflter;
    List<String> items;
    Context context;
    private GetProductByIdTask mAuthTask = null;

    public ProductOfPersonMenuAdapter(Context applicationContext, List<String> items){
        this.context = applicationContext;
        this.items = items;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public String getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.item_productofmenu, null);
        TextView name = (TextView) view.findViewById(R.id.textView4);
        String productId = items.get(i);
        mAuthTask = new GetProductByIdTask(context, name);
        mAuthTask.execute(productId);

        //name.setText(items.get(i));

        return view;
    }

}
