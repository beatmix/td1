package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iproger.td1.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import activity.GroupViewActivity;

import model.Person;
import service.SendBuzzTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 14.11.2016.
 */


public class PersonItemAdapter extends BaseAdapter {

    Context context;
    List<Person> persons;
    public AlertDialog.Builder ad;
    GroupViewActivity parentActivity;
    LayoutInflater inflter;

    Person currentPerson;

    public PersonItemAdapter(Context applicationContext, List<Person> persons, GroupViewActivity parentActivity, Person currentPerson) {
        this.context = applicationContext;
        this.persons = persons;
        this.parentActivity = parentActivity;
        this.currentPerson = currentPerson;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int i) {
        return persons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.item, null);

        TextView nomPrenom = (TextView) view.findViewById(R.id.textView);
        TextView isConn = (TextView) view.findViewById(R.id.textView2);
        ImageButton ib = (ImageButton)view.findViewById(R.id.imageButton);
        ib.setImageResource(R.drawable.minus_btn);
        final ImageButton imageView = (ImageButton) view.findViewById(R.id.imageView);

        nomPrenom.setText(persons.get(i).getFullName());
        isConn.setText(persons.get(i).isConnected());
        setDrawable(imageView, persons.get(i));
        imageView.setTag(i);
        ib.setTag(i);

        imageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if ( persons.get((int)v.getTag()).getConnected().equalsIgnoreCase("false"))
                    Toast.makeText(context, "Serveur est decconecté", Toast.LENGTH_SHORT).show();
                else {
                    if (persons.get((int)v.getTag()).isCalled()==true) {
                            persons.get((int) v.getTag()).setCalled(false);
                        Toast.makeText(context, "Annulé", Toast.LENGTH_SHORT).show();
                    }

                    else{
                    persons.get((int) v.getTag()).setCalled(true);

                    Toast.makeText(context, "Serveur " +persons.get((int)v.getTag()).getFullName()+" est appelé", Toast.LENGTH_SHORT).show();
                        List<Person> personsList = new ArrayList<Person>();
                        personsList.add(persons.get((int)v.getTag()));
                        personsList.add(currentPerson);
                        JSONObject objSender = currentPerson.makeTheJSON();
                        JSONObject objReceiver =persons.get((int)v.getTag()).makeTheJSON();
                        if ((objSender!=null)&&(objReceiver!=null)) {


                            JSONObject obj = new JSONObject();

                            try {
                                obj.put("receiver", objReceiver);
                                obj.put("sender", objSender);
                             //   System.out.println("JSON >>>>>" + obj);
                                AsyncTask tache = new SendBuzzTask(context, parentActivity).execute(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else
                            System.out.println("OBJECTS NULl >>>>>>>>>>>>>");

                        //AsyncTask task = new SendBuzzTask(objSender, objReceiver);
                        //task.execute();


                    }
                    setDrawable(imageView, persons.get((int) v.getTag()));
                }
            }
        });

        ib.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                final String idP = persons.get((int)view.getTag()).getId();
                ad = new AlertDialog.Builder(parentActivity);
                String title = context.getResources().getString(R.string.exit);
                String message = context.getResources().getString(R.string.save_data);;
                String button1String = context.getResources().getString(R.string.yes);;
                String button2String =context.getResources().getString(R.string.no);;


                ad.setTitle(title);
                ad.setMessage(message);
                ad.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {

                        parentActivity.deletePerson(idP, PersonItemAdapter.this, persons);
                    }
                }).setNegativeButton(button2String, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {

                    }
                });
                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                ad.show();

            }
        });

        return view;
    }



    private void setDrawable(ImageButton view, Person p) {


            if (p.getConnected().equalsIgnoreCase("true")) {
                view.setImageResource(R.drawable.button);

                if (p.isCalled() == true) {
                    view.setImageResource(R.drawable.button3);
                }

            } else {
                if (p.getConnected().equalsIgnoreCase("false")) {
                    view.setImageResource(R.drawable.button2);

                }
            }

    }

    public void refresh(List<Person> persons2)
    {
        this.persons = persons2;
        notifyDataSetChanged();
    }





}
