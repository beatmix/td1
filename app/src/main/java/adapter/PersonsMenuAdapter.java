package adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.iproger.td1.R;

import java.io.Serializable;
import java.util.List;

import activity.MenuInfoActivity;
import activity.MenuOfPersonActivity;
import model.PMenu;

/**
 * Created by user on 25.01.2017.
 */

public class PersonsMenuAdapter extends BaseAdapter {

    LayoutInflater inflter;
    Context context;
    List<PMenu> menus;
    MenuOfPersonActivity parentActivity;

    public PersonsMenuAdapter(Context applicationContext, List<PMenu> menus, MenuOfPersonActivity parentActivity) {
        this.context = applicationContext;
        this.menus = menus;
        this.parentActivity = parentActivity;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return menus.size();
    }

    @Override
    public PMenu getItem(int i) {
        return menus.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.personmenu_item, null);
        TextView name = (TextView) view.findViewById(R.id.nameOfMenu);
   //     name.setText(menus.get(i).getAnnounce());
        name.setText(menus.get(i).getCreatedAt().toString());
        ImageButton btn = (ImageButton) view.findViewById(R.id.infoBtn);
        btn.setTag(i);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(parentActivity, MenuInfoActivity.class);
                intent.putExtra("menu_object", (Serializable) menus.get((int)view.getTag()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        return view;
    }

}
